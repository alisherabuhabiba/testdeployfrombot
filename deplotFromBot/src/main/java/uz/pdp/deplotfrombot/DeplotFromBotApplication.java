package uz.pdp.deplotfrombot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeplotFromBotApplication {

    public static void main(String[] args) {
        SpringApplication.run(DeplotFromBotApplication.class, args);
    }

}
